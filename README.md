<p align="center">
  <img src="assets/masterclass-logo.png" title="Logo">
</p>

---

[![platform_ios](assets/platform.png)](https://www.apple.com/ios/ios-11-preview/)
[![ide_xcode9](assets/ide.png)](https://developer.apple.com/xcode)
[![twitter](assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](assets/instagram.png)](https://www.instagram.com/juan_fdorado)

This is a collection of projects and playgrounds with the new stuffs of iOS 12 and Xcode 10

## Table of Contents
1. [Intro To Swift 4](sections/01-intro_to_swift_4) - Learn the Basics of Swift.
2. [Xcode & Autolayouts](sections/02-xcode_and_autolayout) - Take a look of how work `AutoLayouts`.
3. [AutoLayouts And Swift In Practice](sections/03-autolayouts_and_swift_in_practice/bingebot) - Create `BingeBot` and see how `AutoLayouts` make your app looks good.
4. [Intermediate Swift](sections/04-intermediate_swift) - Learn intermediate topics about Swift Language.
5. [Model-View-Controller MVC](sections/05-model_view_controller_mvc/mvc_demo) - Learn the most popular design pattern for iOS Development.
6. TableView and CollectionViews
  * [GoodEatins](sections/06-goodeatins_tableview_and_collectionviews/googeatins) - See how works `UITableViews` and `UICollectionViews`.
  * [Protocols and Delegates](sections/06-goodeatins_tableview_and_collectionviews/protocols_and_delegates) - Simple example of how and what is a `Protocol` and a `Delegate`.
7. [Web Requests And API's](sections/07-web_requests_and_apis/startrivia) - Learn about Web Request with Star Wars.
8. ARKit
  * [Intergalactic-Fun](sections/08-arkit/intergalactic_fun) - Learn the basics to create a basic `ARKit` with `Geometries`, `Rotations`, `Movements`.
