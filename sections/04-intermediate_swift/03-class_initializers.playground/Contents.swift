import UIKit

// MARK: Class Initializers

class Developer {
    
    var firstName: String
    var lastName: String
    var position = "Junior Developer"
    var salary: Double {
        willSet {
            if newValue >= 120000 {
                position = "Senior Developer"
            }
        }
        
        didSet {
            
        }
    }
    
    var monthlySalary: Double {
        get {
            return self.salary / 12
        }
        
        set {
            salary = newValue * 12
        }
    }
    
    init() {
        self.firstName = ""
        self.lastName = ""
        self.salary = 0
    }
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.salary = 0
        
        debugPrint("Hi \(self.firstName) \(self.lastName), you are contracted with \(self.salary)k at year")
    }
    
    init(firstName: String, lastName: String, salary: Double) {
        self.firstName = firstName
        self.lastName = lastName
        self.salary = salary
        
        debugPrint("Hi \(self.firstName) \(self.lastName), you are contracted with \(self.salary)k at year")
    }
    
    func requestRaise(inTheAmountOf amount: Double) {
        salary += amount
    }
}

let dev = Developer(firstName: "Juan", lastName: "Dorado")
let devNew = Developer(firstName: "Luke", lastName: "SK", salary: 100000)

devNew.position
devNew.requestRaise(inTheAmountOf: 10000)
devNew.position

devNew.requestRaise(inTheAmountOf: 10000)
devNew.position


// MARK: Structures

struct DevStruct {
    
    var firstName: String
    var lastName: String
    var position = "Junior Developer"
    var salary: Double {
        willSet {
            if newValue >= 120000 {
                position = "Senior Developer"
            }
        }
        
        didSet {
            
        }
    }
    
    var monthlySalary: Double {
        get {
            return self.salary / 12
        }
        
        set {
            salary = newValue * 12
        }
    }
    
    init() {
        self.firstName = ""
        self.lastName = ""
        self.salary = 0
    }
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.salary = 0
        
        debugPrint("Hi \(self.firstName) \(self.lastName), you are contracted with \(self.salary)k at year")
    }
    
    init(firstName: String, lastName: String, salary: Double) {
        self.firstName = firstName
        self.lastName = lastName
        self.salary = salary
        
        debugPrint("Hi \(self.firstName) \(self.lastName), you are contracted with \(self.salary)k at year")
    }
}

let devStruct = DevStruct(firstName: "Juan", lastName: "Dorado", salary: 10)

// MARK: DIFERENCES
/*
 Structures are VALUE types: This means that when we pass instances of a structure within our app, we are passing a COPY of the structure and not the original structure. So changes can be made to the pass structure without any changes occurring to the original struct.
 
 Classes are REFERENCE types: This means when we pass an instance of a class, say from one view controler to another, we are passing a reference to the original instance. This means that any changes made to the passed instance will also be applied to the first instance.
 */
