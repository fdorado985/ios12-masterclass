//
//  Starship.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Starship: Codable {
    
    // MARK: Properties
    
    let name: String
    let model: String
    let maker: String
    let cost: String
    let length: String
    let speed: String
    let crew: String
    let passengers: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case model
        case maker = "manufacturer"
        case cost = "cost_in_credits"
        case length
        case speed = "max_atmosphering_speed"
        case crew
        case passengers
    }
}
