# GoodEatins
It is a cute app where you are gonna be able to see how works `UITableViews`, `UICollectionViews` and also the `Delegate` and `DataSource` of each.

You are gonna see a list of some different food categories and recipes on each category of different kind of foods.

## Demo
![goodeatins_demo](.screenshots/goodeatins_demo.gif)
