import UIKit

// MARK: Dictionaries

// Create
var emptyExplicitDevices: Dictionary<String, Double> = [:] // Empty explicit dictionary
var emptyDevices = Dictionary<String, Double>() // Empty implicit dictionary
var screenSizesInches: Dictionary<String, Double> = ["iPhone 7" : 4.7, "iPhone 7 Plus" : 5.2, "iPad Pro" : 12.9]

// Access
screenSizesInches["iPhone 7"]

// Modify
screenSizesInches["iPhone 7 Plus"] = 5.5

// Add
screenSizesInches["iPad Air 2"] = 9.7

// Remove
screenSizesInches.removeValue(forKey: "iPad Air 2")
