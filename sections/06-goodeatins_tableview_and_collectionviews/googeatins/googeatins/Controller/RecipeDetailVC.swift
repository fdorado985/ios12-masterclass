//
//  RecipeDetailVC.swift
//  googeatins
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RecipeDetailVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet private weak var imgRecipe: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblInstructions: UILabel!
    
    // MARK: Properties
    
    var selectedRecipe: Recipe?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let selectedRecipe = self.selectedRecipe else { fatalError("selectedRecipe is null") }
        lblTitle.text = selectedRecipe.title
        lblInstructions.text = selectedRecipe.instructions
        imgRecipe.image = UIImage(named: selectedRecipe.imageName)
    }

}
