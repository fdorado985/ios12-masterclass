//
//  ViewController.swift
//  bingebot
//
//  Created by Juan Francisco Dorado Torres on 9/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: Properties
    
    @IBOutlet weak var showsLabel: UILabel!
    @IBOutlet weak var randomShowLabel: UILabel!
    @IBOutlet weak var bingebotSpokenLabel: UILabel!
    @IBOutlet weak var addShowTextField: UITextField!
    @IBOutlet weak var addShowButton: UIButton!
    @IBOutlet weak var randomShowStackView: UIStackView!
    @IBOutlet weak var addShowStackView: UIStackView!
    @IBOutlet weak var showsStackView: UIStackView!
    @IBOutlet weak var selectionSegment: UISegmentedControl!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: Properties
    
    private var shows: [String] = []
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addShowButton.layer.borderWidth = 2.0
        addShowButton.layer.borderColor = UIColor.white.cgColor
        addShowButton.layer.cornerRadius = 20
        
        cleanView()
    }
    
    // MARK: IBActions

    @IBAction func addShowBtnWasPressed(_ sender: Any) {
        guard let showName = addShowTextField.text, !showName.isEmpty else { return }
        shows.append(showName)
        updateShowsLabel()
        addShowTextField.text = ""
        showsStackView.isHidden = false
        
        if shows.count > 1 {
            randomShowStackView.isHidden = false
            bingebotSpokenLabel.isHidden = true
            randomShowLabel.isHidden = true
        }
    }
    
    @IBAction func randomBingeShowBtnWasPressed(_ sender: Any) {
        randomShowLabel.text = shows.randomElement()
        randomShowLabel.isHidden = false
        bingebotSpokenLabel.isHidden = false
    }
    
    @IBAction func selectionSegmentValueChanged(_ sender: Any) {
        switch selectionSegment.selectedSegmentIndex {
        case 0:
            titleLabel.text = "SHOWS"
            addShowButton.setTitle("ADD SHOW", for: .normal)
            bingebotSpokenLabel.text = "BINGEBOT HAS SPOKEN, YOU SHOULD WATCH:"
        case 1:
            titleLabel.text = "RESTAURANTS"
            addShowButton.setTitle("ADD RESTAURANT", for: .normal)
            bingebotSpokenLabel.text = "BINGEBOT HAS SPOKEN, YOU SHOULD GO TO:"
        default:
            break
        }
        cleanView()
    }
    
    // MARK: Private
    
    private func updateShowsLabel() {
        showsLabel.text = shows.joined(separator: ", ")
    }
    
    private func cleanView() {
        showsStackView.isHidden = true
        randomShowStackView.isHidden = true
        shows = []
    }
}

