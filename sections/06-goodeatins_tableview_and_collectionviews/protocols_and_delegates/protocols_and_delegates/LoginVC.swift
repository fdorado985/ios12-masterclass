//
//  LoginVC.swift
//  protocols_and_delegates
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var txtUsername: UITextField!
    
    // MARK: Properties
    
    var delegate: LoginCompleteDelegate?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: IBActions
    
    @IBAction func btnActionTapped(_ sender: UIButton) {
        guard let username = txtUsername.text, !username.isEmpty else { return }
        delegate?.userCreate(name: username)
        dismiss(animated: true)
    }
}
