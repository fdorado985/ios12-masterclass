import UIKit

// MARK: Enumerations

enum SubType: Int {
    case none = 0
    case bronze
    case silver
    case gold
}

var subtype = SubType.silver
var subtypeTwo: SubType = SubType.silver
var subtypeThree: SubType = .silver

if subtype == .silver {
    // perform some action for silver subs
    debugPrint("This user is a silver")
}

switch subtype {
case .none:
    debugPrint("This user has no subtype")
case .bronze:
    debugPrint("This user is bronze subtype")
case .silver:
    debugPrint("This user is silver subtype")
case .gold:
    debugPrint("This user is gold subtype")
}

switch subtype {
case .none:
    debugPrint("This user has no subtype")
default:
    debugPrint("We don't care the other values")
}


// downloaded user account and subtype property = 2
var subscriberType = 2
var type: SubType

switch subscriberType {
case 1:
    subtype = .bronze
case 2:
    subtype = .silver
case 3:
    subtype = .gold
default:
    subtype = .none
}
