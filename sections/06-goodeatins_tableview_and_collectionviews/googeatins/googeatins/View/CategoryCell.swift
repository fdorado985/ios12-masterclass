//
//  CategoryCell.swift
//  googeatins
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    // MARK: IBOutlets

    @IBOutlet private weak var imgCategory: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCategory.layer.cornerRadius = 10
    }
    
    // MARK: Public
    
    func configureCell(category: FoodCategory) {
        imgCategory.image = UIImage(named: category.imageName)
        lblTitle.text = category.title
    }
}
