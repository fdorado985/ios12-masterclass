import UIKit

// MARK: - OPTIONALS

var optionalString: String?
var optional: Optional<String>
debugPrint(optionalString)

optionalString = "JonnyD"
debugPrint(optionalString)

// Unwrapping
// Force (Only when you are 100% there is a value)
var a: Int?
var b: Int?

// let sum  = a! + b! // Unexpectedly found nil while unwrapping an Optional value

a = 4
b = 6

let sum  = a! + b!
debugPrint(sum)

// Optional Binding
if let number = a {
    // here we can use number
    debugPrint(number)
}

// Guard Let
func getNumberA() {
    guard let numberA = a else { return }
    debugPrint(numberA)
    
    guard let numberB = b else { return }
    debugPrint(numberA + numberB)
    
    guard let a = a, let b = b else { return }
    debugPrint(a + b)
}

getNumberA()

// Nil Coalescing Operator
var name: String? = "Jonny"
debugPrint(name ?? "Bill")

var actualName = name ?? "Fred"
debugPrint(actualName)
