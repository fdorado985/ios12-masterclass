#  Protocols & Delegates

## Delegates
Delegation is a design pattern that enables a class or structure to hand off (or delegate) some of its responsibilities to an instance of another type... followed a set of tules that explain the correct conduct and procedures to be followed in specific situations.

## Protocols
A protocol defines a blueprint of methods, properties, and other requirements that suit a particular task or piece of functionality. The protocol can then be adopted by a class, structure, or enumeration to provide an actual implementation of those requirements. Any type that satisfies the requirements of a protocol is said to conform to that protocol.

## Demo
![protocols_and_delegates_demo](.screenshots/protocols_and_delegates_demo.gif)
