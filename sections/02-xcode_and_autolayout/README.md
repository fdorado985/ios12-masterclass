# Xcode & Autolayout

## Xcode Tour - Soon

## Images

Xcode handle 3 different kind of sizes for images... this is because of the improvements on  the iPhone Screens, these are...

* 1x : Standard - It means 1 px
* 2x : Retina - It means 4 px
* 3x : Super Retina - It means 9 px

For now not all of the iPhones hold the 1x size... you can help yourself with this image.

![iphone_sizes](.assets/iphone_sizes.png)

For full information feel free to look at the Apple's documentation:
* [iOS Device Compatibility Reference: Displays](https://developer.apple.com/library/archive/documentation/DeviceInformation/Reference/iOSDeviceCompatibility/Displays/Displays.html)
* [Human Interface Guidelines: Image Size and Resolutions](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/image-size-and-resolution/)

Also if you look for Free Resources you can take a look at here.

* Images
  * [Pexels](pexels.com) : Free, non-attribution
  * [Pixalbay](pixalbay.com) : Free, non-attribution
* Vector Icons
  * [The Noun Project](thenounproject.com) : Free, attribution
* UI Design Resources & Inspiration
  * [Sketch App Sources](sketchappsources.com) : Free, terms per asset
  * [Dribbble](dribbble.com) : For Inspiration
* Videos
  * [Videos Pexels](videos.pexels.com) : Free, non-attribution
  * [Coverr](coverr.co) : Free, non-attribution

## Create App Images

To create the app images you can use these three ways.

1. [Sketch](https://www.sketchapp.com)
2. [Iconizer](http://raphaelhanneken.github.io/iconizer/)
3. [AppIcon](https://appicon.co)


## Autolayout

Autolayout is an amazing feature included in Xcode to let us adapt our screen design to the different sizes of iPhones... from 4s (deprecated) to Xs MAX.

* [Learning Autolayout](learning_autolayout) - Take a look to different `ViewControllers` to see some examples of how `Autolayouts` work.
* [ScrollViews](scrollviews) - Add ScrollViews to a group of views to make it look good on small iPhone sizes.
