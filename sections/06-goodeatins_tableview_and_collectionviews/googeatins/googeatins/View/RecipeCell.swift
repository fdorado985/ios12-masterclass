//
//  RecipeCell.swift
//  googeatins
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RecipeCell: UICollectionViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var imgRecipe: UIImageView!
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgRecipe.layer.cornerRadius = 10
    }
    
    // MARK: Public
    
    func configureCell(recipe: Recipe) {
        self.imgRecipe.image = UIImage(named: recipe.imageName)
    }
}
