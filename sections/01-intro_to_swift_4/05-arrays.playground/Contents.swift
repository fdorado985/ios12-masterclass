import UIKit

// MARK: Arrays

// Create an array
var cards = ["Ace", "King", "Queen", "Joker"]

// Access - The elements are counted from zero
cards[0]

// Add
cards.append("Jack")

// Remove
cards.remove(at: 1) // Value King should be removed

// Insert in specific position
cards.insert("King", at: 3)

// Modify Items
cards[1] = "Jester"


// Example 2
let fingers: [String] = []
// fingers.append("Thumb")
// fingers.append("Middle")

// ...

// Cannot modify a constant array
