//
//  CustomView.swift
//  mvc_demo
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CustomView: UIView {
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 5
        layer.cornerRadius = 15
        clipsToBounds = true
    }

}
