# Model View Controller
Model View Controller is a design pattern commonly used on iOS Development, of course you can adapt your development to any Design Pattern, but this is the one that is by default.

>**What is a design patter?** : A general repeatable solution to a commonly occurring problem in >software design

Model View Controller is divided by its own name
* **Model**: Has the access to all the data to be able to play with it to let it be presented as required
* **View**: It is the interface, all the user can see on its phone
* **Controller**: It holds the events of the view, when tap, swipe, or do any action.

## Demo
![mvc_demo](.screenshots/mvc_demo.gif)
