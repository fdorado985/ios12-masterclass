//
//  PlanetViewerVC.swift
//  intergalactic_fun
//
//  Created by Juan Francisco Dorado Torres on 11/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class PlanetViewerVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet var sceneView: ARSCNView!
    
    // MARK: Properties
    
    var planetName: String!
    private let baseNode = SCNNode()
    private let planetNode = SCNNode()
    private let textNode = SCNNode()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        sceneView.debugOptions = [.showFeaturePoints/*, .showBoundingBoxes*/]
        addBaseNode()
        addPlanet()
        addText()
        addShip()
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(dismiss(from:)))
        sceneView.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    // MARK: Functions
    
    private func addBaseNode() {
        let baseLocation = SCNVector3(0.0, 0.0, -1.0)
        baseNode.position = baseLocation
        sceneView.scene.rootNode.addChildNode(baseNode)
    }
    
    private func addPlanet() {
        let planet = SCNSphere(radius: 0.3)
        let material = SCNMaterial()
        material.diffuse.contents = UIImage(named: planetName)
        planet.materials = [material]
        planetNode.geometry = planet
        baseNode.addChildNode(planetNode)
        
        // Add rotation
        let planetRotate = SCNAction.rotateBy(x: 0, y: 2 * .pi, z: 0, duration: 25)
        let repeatRotate = SCNAction.repeatForever(planetRotate)
        planetNode.runAction(repeatRotate)
    }
    
    private func addText() {
        let text = SCNText(string: planetName.capitalized, extrusionDepth: 1)
        text.font = UIFont(name: "futura", size: 16)
        text.flatness = 0
        
        let scaleFactor = 0.1 / text.font.pointSize
        textNode.scale = SCNVector3(scaleFactor, scaleFactor, scaleFactor)
        textNode.geometry = text
        
        let max = textNode.boundingBox.max.x
        let min = textNode.boundingBox.min.x
        let midPoint = -((max - min) / 2 + min) * Float(scaleFactor)
        
        textNode.position = SCNVector3(midPoint, 0.35, 0)
        baseNode.addChildNode(textNode)
    }
    
    private func addShip() {
        // Create the base rotate node for the ship
        let rotateNode = SCNNode()
        baseNode.addChildNode(rotateNode)
        
        // Look for the ship on the scene to add it to the rotate node
        guard let scene = SCNScene(named: "art.scnassets/ship.scn") else { fatalError() }
        guard let shipNode = scene.rootNode.childNode(withName: "ship", recursively: true) else { fatalError() }
        shipNode.scale = SCNVector3(0.2, 0.2, 0.2)
        shipNode.position = SCNVector3(-0.35, 0, 0)
        rotateNode.addChildNode(shipNode)
        
        // Create the orbit to move the ship around the planet
        let orbitAction = SCNAction.rotateBy(x: 0, y: 2 * .pi, z: 0, duration: 6)
        let repeatOrbit = SCNAction.repeatForever(orbitAction)
        rotateNode.runAction(repeatOrbit)
        
        // ShipUpDownAction - Example 1
        /*let shipUpAction = SCNAction.move(to: SCNVector3(-0.35, 0.2, 0), duration: 2)
        shipUpAction.timingMode = .easeInEaseOut
        let shipDownAction = SCNAction.move(to: SCNVector3(-0.35, -0.2, 0), duration: 2)
        shipDownAction.timingMode = .easeInEaseOut
        let upDown = SCNAction.sequence([shipUpAction, shipDownAction])
        let repeatUpDown = SCNAction.repeatForever(upDown)
        shipNode.runAction(repeatUpDown)*/
        
        // PitchUpDownAction - Example 2
        let wait = SCNAction.wait(duration: 1)
        let pitchUpAction = SCNAction.rotateTo(x: -0.3, y: 0.0, z: -0.17, duration: 1)
        pitchUpAction.timingMode = .easeInEaseOut
        let levelOff = SCNAction.rotateTo(x: 0.0, y: 0.0, z: 0.0, duration: 1)
        let shipReversePitch = SCNAction.rotateTo(x: 0.3, y: 0.0, z: 0.17, duration: 1)
        shipReversePitch.timingMode = .easeInEaseOut
        let pitchUpDown = SCNAction.sequence([pitchUpAction, levelOff, wait, shipReversePitch, levelOff, wait])
        let repeatPitch = SCNAction.repeatForever(pitchUpDown)
        shipNode.runAction(repeatPitch)
    }
    
    @objc private func dismiss(from gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .right {
            dismiss(animated: true)
        }
    }
}

// MARK: - ARSCNViewDelegate

extension PlanetViewerVC: ARSCNViewDelegate { }
