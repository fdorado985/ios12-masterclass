//
//  HomeworldApi.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire

class HomeworldApi {
    
    // MARK: Public - Alamofire & Codable
    
    func getHomeworld(url: String, completion: @escaping HomeworldResponseCompletion) {
        guard let url = URL(string: url) else { fatalError("\(#function) : no URL casted") }
        Alamofire.request(url).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let data = response.data else {
                debugPrint("\(#function): Cannot cast data response")
                completion(nil)
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let homeworld = try jsonDecoder.decode(Homeworld.self, from: data)
                completion(homeworld)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
        }
    }
}
