//
//  RoundedImage.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RoundedImage: UIImageView {
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = frame.height / 2
    }

}
