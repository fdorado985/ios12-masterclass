//
//  VehiclesVC.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class VehiclesVC: UIViewController, PersonProtocol {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var lblModel: UILabel!
    @IBOutlet private weak var lblMaker: UILabel!
    @IBOutlet private weak var lblCost: UILabel!
    @IBOutlet private weak var lblLength: UILabel!
    @IBOutlet private weak var lblSpeed: UILabel!
    @IBOutlet private weak var lblCrew: UILabel!
    @IBOutlet private weak var lblPassengers: UILabel!
    @IBOutlet private weak var btnNext: FadeEnableBtn!
    @IBOutlet private weak var btnPrevious: FadeEnableBtn!
    
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingSpinner: UIActivityIndicatorView!
    
    // MARK: Properties
    
    var person: Person?
    private let api = VehiclesApi()
    private var vehicles = [String]()
    private var currentVehicle = 0
    
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let person = person else { fatalError("\(#function): Person is nil") }
        guard person.vehicleUrls.count > 0 else { fatalError("\(#function): Person doesn't have vehicles") }
        
        self.vehicles = person.vehicleUrls
        setButtonState()
        downloadVehicle(for: vehicles[currentVehicle])
    }
    
    // MARK: IBActions
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        currentVehicle += 1
        setButtonState()
        downloadVehicle(for: vehicles[currentVehicle])
    }
    
    @IBAction func btnPreviousTapped(_ sender: UIButton) {
        currentVehicle -= 1
        setButtonState()
        downloadVehicle(for: vehicles[currentVehicle])
    }
    
    // MARK: Private
    
    private func updateUI(vehicle: Vehicle) {
        lblName.text = vehicle.name
        lblModel.text = vehicle.model
        lblMaker.text = vehicle.maker
        lblCost.text = vehicle.cost
        lblLength.text = vehicle.length
        lblSpeed.text = vehicle.speed
        lblCrew.text = vehicle.crew
        lblPassengers.text = vehicle.passengers
        loading(isEnabled: false)
    }
    
    private func downloadVehicle(for vehicleUrl: String) {
        loading(isEnabled: true)
        api.getVehicles(url: vehicles[currentVehicle]) { (vehicle) in
            if let vehicle = vehicle {
                DispatchQueue.main.async {
                    self.updateUI(vehicle: vehicle)
                }
            }
        }
    }
    
    private func setButtonState() {
        btnPrevious.isEnabled = currentVehicle == 0 ? false : true
        btnNext.isEnabled = currentVehicle == vehicles.count - 1 ? false : true
    }
    
    private func loading(isEnabled: Bool) {
        if isEnabled {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 1.0
                self.loadingView.isHidden = false
                self.loadingSpinner.startAnimating()
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 0.0
                self.loadingView.isHidden = true
                self.loadingSpinner.stopAnimating()
            }
        }
    }
}
