# Intro To Swift 4

Here you will see a couple of examples divided by playgrounds to see some of the basics on Swift 4

## Table of Contents
* Variables & Constants
* Strings
* Numbers
* Booleans
* Arrays
* Dictionaries
* Functions
* Control Flow & Loops
