//
//  Homeworld.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Homeworld: Codable {
    
    // MARK: Properties
    
    let name: String
    let climate: String
    let terrain: String
    let population: String
}
