//
//  RecipesSelectionVC.swift
//  googeatins
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RecipesSelectionVC: UIViewController {
    
    // MARK: IBActions
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Properties
    
    var selectedCategory: FoodCategory?
    private var recipes = [Recipe]()
    private let data = DataSet()
    private var selectedRecipe: Recipe?
    
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        guard let selectedCategoryTitle = selectedCategory?.title else { fatalError("NO SELECTED CATEGORY") }
        recipes = data.getRecipes(forCategoryTitle: selectedCategoryTitle)
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let recipeDetailVC = segue.destination as? RecipeDetailVC else { fatalError("RecipeDetailVC was not found") }
        guard let selectedRecipe = self.selectedRecipe else { fatalError("RECIPE WAS NOT SELECTED") }
        recipeDetailVC.selectedRecipe = selectedRecipe
    }
}

extension RecipesSelectionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecipeCell", for: indexPath) as? RecipeCell else { fatalError("RecipeCell not found")}
        cell.configureCell(recipe: recipes[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedRecipe = recipes[indexPath.item]
        performSegue(withIdentifier: "RecipeDetailVC", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.width
        let cellDimension = (width / 2) - 15
        return CGSize(width: cellDimension, height: cellDimension)
    }
}
