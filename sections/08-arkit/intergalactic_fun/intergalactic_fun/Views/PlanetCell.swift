//
//  PlanetCell.swift
//  intergalactic_fun
//
//  Created by Juan Francisco Dorado Torres on 11/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PlanetCell: UITableViewCell {

    // MARK: IBOutlets
    
    @IBOutlet weak private var imgBackground: UIImageView!
    @IBOutlet weak private var lblTitle: UILabel!
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgBackground.layer.cornerRadius = 10
    }
    
    // MARK: Functions
    
    func configureCell(planet: String) {
        lblTitle.text = planet.capitalized
        imgBackground.image = UIImage(named: planet)
    }

}
