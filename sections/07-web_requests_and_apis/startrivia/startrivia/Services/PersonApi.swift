//
//  PersonApi.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PersonApi {
    
    // MARK: Public - NSURLSession & Manual Parsing
    
    func getRandomPersonURLSession(id: Int, completion: @escaping PersonResponseCompletion) {
        guard let url = URL(string: "\(PERSON_URL)\(id)") else { fatalError("\(#function) : no URL casted") }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let data = data else { return }
            do {
                let rawJson = try JSONSerialization.jsonObject(with: data, options: [])
                guard let json = rawJson as? [String : Any] else { return }
                let person = self.parsePersonManual(json: json)
                completion(person)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
        }
        task.resume()
    }
    
    // MARK: Public - Alamofire & SwiftyJSON
    
    func getRandomPersonAlamofire(id: Int, completion: @escaping PersonResponseCompletion) {
        guard let url = URL(string: "\(PERSON_URL)\(id)") else { fatalError("\(#function) : no URL casted") }
        Alamofire.request(url).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let jsonData = response.data else {
                debugPrint("\(#function): Cannot cast json response")
                completion(nil)
                return
            }
            
            do {
                let json = try JSON(data: jsonData)
                let person = self.parsePerson(json: json)
                completion(person)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
        }
    }
    
    // MARK: Public - Alamofire & Codable
    
    func getRandomPerson(id: Int, completion: @escaping PersonResponseCompletion) {
        guard let url = URL(string: "\(PERSON_URL)\(id)") else { fatalError("\(#function) : no URL casted") }
        Alamofire.request(url).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let jsonData = response.data else {
                debugPrint("\(#function): Cannot cast json response")
                completion(nil)
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let person = try jsonDecoder.decode(Person.self, from: jsonData)
                completion(person)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
        }
    }
    
    // MARK: Private
    
    // MARK: Parse Manual
    
    private func parsePersonManual(json: [String : Any]) -> Person {
        let name = json["name"] as? String ?? ""
        let height = json["height"] as? String ?? ""
        let mass = json["mass"] as? String ?? ""
        let hair = json["hair_color"] as? String ?? ""
        let birthYear = json["birth_year"] as? String ?? ""
        let gender = json["gender"] as? String ?? ""
        let homeworldUrl = json["homeworld"] as? String ?? ""
        let filmUrls = json["films"] as? [String] ?? [String]()
        let vehicleUrls = json["vehicles"] as? [String] ?? [String]()
        let starshipUrls = json["starships"] as? [String] ?? [String]()
        
        let person = Person(name: name, height: height, mass: mass, hair: hair, birthYear: birthYear, gender: gender, homeworldUrl: homeworldUrl, filmUrls: filmUrls, vehicleUrls: vehicleUrls, starshipUrls: starshipUrls)
        return person
    }
    
    // MARK: Parse Swifty JSON
    
    private func parsePerson(json: JSON) -> Person {
        let name = json["name"].stringValue
        let height = json["height"].stringValue
        let mass = json["mass"].stringValue
        let hair = json["hair_color"].stringValue
        let birthYear = json["birth_year"].stringValue
        let gender = json["gender"].stringValue
        let homeworldUrl = json["homeworld"].stringValue
        let filmUrls = json["films"].arrayValue.map { $0.stringValue }
        let vehicleUrls = json["vehicles"].arrayValue.map { $0.stringValue }
        let starshipUrls = json["starships"].arrayValue.map { $0.stringValue }
        
        let person = Person(name: name, height: height, mass: mass, hair: hair, birthYear: birthYear, gender: gender, homeworldUrl: homeworldUrl, filmUrls: filmUrls, vehicleUrls: vehicleUrls, starshipUrls: starshipUrls)
        return person
    }
}
