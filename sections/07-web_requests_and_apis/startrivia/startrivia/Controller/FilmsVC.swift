//
//  FilmsVC.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class FilmsVC: UIViewController, PersonProtocol {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblEpisode: UILabel!
    @IBOutlet private weak var lblDirector: UILabel!
    @IBOutlet private weak var lblProducer: UILabel!
    @IBOutlet private weak var lblReleased: UILabel!
    @IBOutlet private weak var txtCrawl: UITextView!
    @IBOutlet private weak var btnNext: FadeEnableBtn!
    @IBOutlet private weak var btnPrevious: FadeEnableBtn!
    
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingSpinner: UIActivityIndicatorView!
    
    // MARK: Properties
    
    var person: Person?
    private let api = FilmsApi()
    private var films = [String]()
    private var currentFilm = 0
    
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let person = person else { fatalError("\(#function): Person is nil") }
        guard person.filmUrls.count > 0 else { fatalError("\(#function): Person doesn't have films") }
        
        self.films = person.filmUrls
        setButtonState()
        downloadFilm(for: films[currentFilm])
    }
    
    // MARK: IBActions
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        currentFilm += 1
        setButtonState()
        downloadFilm(for: films[currentFilm])
    }
    
    @IBAction func btnPreviousTapped(_ sender: UIButton) {
        currentFilm -= 1
        setButtonState()
        downloadFilm(for: films[currentFilm])
    }
    
    // MARK: Private
    
    private func updateUI(film: Film) {
        lblTitle.text = film.title
        lblEpisode.text = "\(film.episode)"
        lblDirector.text = film.director
        lblProducer.text = film.producer
        lblReleased.text = film.released
        txtCrawl.text = film.crawl
            .replacingOccurrences(of: "\n", with: " ")
            .replacingOccurrences(of: "\r", with: "")
        loading(isEnabled: false)
    }
    
    private func downloadFilm(for filmUrl: String) {
        loading(isEnabled: true)
        api.getFilm(from: films[currentFilm]) { (film) in
            if let film = film {
                DispatchQueue.main.async {
                    self.updateUI(film: film)
                }
            }
        }
    }
    
    private func setButtonState() {
        btnPrevious.isEnabled = currentFilm == 0 ? false : true
        btnNext.isEnabled = currentFilm == films.count - 1 ? false : true
    }
    
    private func loading(isEnabled: Bool) {
        if isEnabled {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 1.0
                self.loadingView.isHidden = false
                self.loadingSpinner.startAnimating()
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 0.0
                self.loadingView.isHidden = true
                self.loadingSpinner.stopAnimating()
            }
        }
    }
}
