import UIKit

// MARK: Variables & Constants

// MARK: Variables
// String | Numbers | Booleans | Collections

var string = "Hello, playground" // Implicit
var stringTwo: String = "Hello" // Explicit

string = "Hello, student"

print(string)

// Integers

var bankBalance = 1000 // Implicit
var ethereum = 290

bankBalance = bankBalance - ethereum
print(bankBalance)

// Booleans : True | False

var amIAwesome = true
amIAwesome = false

if amIAwesome == true {
    // Do something awesome
    print("I am awesome!")
} else {
    // Do something not awesome
    print("I am not awesome...")
}

amIAwesome = !amIAwesome

// MARK: Constants

let age = 16

if age >= 21 {
    print("Time to drink some fancy drinks")
} else {
    print("Head home kid, you're too young")
}
