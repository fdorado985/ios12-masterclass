//
//  Film.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Film: Codable {
    
    // MARK: Properties
    
    let title: String
    let episode: Int
    let director: String
    let producer: String
    let released: String
    let crawl: String
    
    enum CodingKeys: String, CodingKey {
        case title
        case episode = "episode_id"
        case director
        case producer
        case released = "release_date"
        case crawl = "opening_crawl"
    }
}
