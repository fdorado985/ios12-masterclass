import UIKit

// MARK: Booleans

var isLightOn = true

// Unary Operator

isLightOn = !isLightOn

// Binary Operator

var isBatmanAmazing = true
var name = "John"
var year = 2018

// Ternary Operator

var isItDark = false
isItDark = isLightOn ? false : true
