//
//  Movie.swift
//  mvc_demo
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Movie {
    
    private(set) var title: String
    private(set) var description: String
    private(set) var detailedDescription: String
}
