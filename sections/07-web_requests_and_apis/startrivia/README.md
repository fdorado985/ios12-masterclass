#  StarTrivia

Fan of StarWars? Well... here you'll see a simple Trivia about Star Wars... where you can take Randomly a character and see its Homeworld, Vehicles, Spaceships and Films.

Here also, talking about code... you will see...
* `Cocoapods`
* `Alamofire`
* `Codable`
* `SwiftyJSON`
* `URL Requests`
* `JSON Parsing`
* `NSURLSession`

So take a look and have fun with Star Wars.

## Demo
![startrivia_demo](.screenshots/startrivia_demo.gif)
