//
//  FilmsApi.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire

class FilmsApi {
    
    // MARK: Public - Alamofire & Codable
    
    func getFilm(from url: String, completion: @escaping FilmResponseCompletion) {
        guard let url = URL(string: url) else { fatalError("\(#function) : no URL casted") }
        Alamofire.request(url).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let data = response.data else {
                debugPrint("\(#function): Cannot cast data response")
                completion(nil)
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let film = try jsonDecoder.decode(Film.self, from: data)
                completion(film)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(nil)
                return
            }
        }
    }
}
