//
//  ViewController.swift
//  mvc_demo
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDetailedDescription: UILabel!
    
    // MARK: Properties
    
    let movie = Movie(title: incrediblesTitle,
                      description: incrediblesDescription,
                      detailedDescription: incrediblesDetailedDescription)
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: IBAction
    
    @IBAction func btnUpdateViewTapped(_ sender: UIButton) {
        lblTitle.text = movie.title
        lblDescription.text = movie.description
        lblDetailedDescription.text = movie.detailedDescription
    }
}

