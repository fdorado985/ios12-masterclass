//
//  Constants.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

// Service
let URL_BASE = "https://swapi.co/api/"
let PERSON_URL = "\(URL_BASE)people/"

// Colors
let BLACK_BG = UIColor.black.withAlphaComponent(0.6).cgColor

// Closures
typealias PersonResponseCompletion = (Person?) -> Void
typealias HomeworldResponseCompletion = (Homeworld?) -> Void
typealias VehicleResponseCompletion = (Vehicle?) -> Void
typealias StarshipResponseCompletion = (Starship?) -> Void
typealias FilmResponseCompletion = (Film?) -> Void
