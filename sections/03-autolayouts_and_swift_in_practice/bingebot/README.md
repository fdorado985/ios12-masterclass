#  BingeBot

See the basics of using `IBActions`, `IBOutlets`, `Functions`, `Variables`, and more important... `Autolayout` using `StackViews`
BingeBot is a simple app that will help you to decide between shows or restaurants, you don't know where to go... what to watch? Well... let BingeBot decide for you! 🤖

## Demo
![bingebot_demo](.screenshots/bingebot_demo.gif)
