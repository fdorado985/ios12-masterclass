#  Intergalactic Fun
This simple `ARKit` app is a basic example of how to use simple `textures`, add `Spheres`, `Text`, get a `Node` from a `Scene`, and some funny `actions` as `move`, `rotate`

## Demo
![intergalactic_fun_demo](.screenshots/intergalactic_fun_demo.gif)
