//
//  Incredibles.swift
//  mvc_demo
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

let incrediblesTitle = "Incredibles 2"
let incrediblesDescription = "Everyone's favorite family of superheroes is back."
let incrediblesDetailedDescription = "Incredibles 2 is a 2018 American 3D computed0aniated superhero film produced by Pixar Animation Studios and released by Walt Disney Pictures"
