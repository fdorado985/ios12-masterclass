//
//  ViewController.swift
//  protocols_and_delegates
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var lblUsername: UILabel!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? LoginVC else { return }
        destination.delegate = self
    }
}

extension HomeVC: LoginCompleteDelegate {
    
    func userCreate(name: String) {
        lblUsername.text = name
    }
}

