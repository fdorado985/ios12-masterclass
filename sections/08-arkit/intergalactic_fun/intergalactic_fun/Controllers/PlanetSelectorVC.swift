//
//  PlanetSelectorVC.swift
//  intergalactic_fun
//
//  Created by Juan Francisco Dorado Torres on 11/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PlanetSelectorVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak private var tablePlanets: UITableView!
    
    // MARK: Properties
    
    private var planets = ["sun", "mercury", "venus", "mars", "jupiter", "saturn", "uranus", "neptune", "earth-night", "moon"]
    private var selectedPlanet: String?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tablePlanets.delegate = self
        tablePlanets.dataSource = self
        tablePlanets.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
}

// MARK: - UITableViewDelegate && UITableViewDataSource

extension PlanetSelectorVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlanetCell", for: indexPath) as? PlanetCell else { fatalError() }
        
        cell.configureCell(planet: planets[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPlanet = planets[indexPath.row]
        performSegue(withIdentifier: "PlanetViewerVC", sender: self)
    }
}

// MARK: - Navigation

extension PlanetSelectorVC {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let planetVC = segue.destination as? PlanetViewerVC,
            let selectedPlanet = self.selectedPlanet {
            planetVC.planetName = selectedPlanet
        }
    }
}
