//
//  StarshipsVC.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class StarshipsVC: UIViewController, PersonProtocol {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var lblModel: UILabel!
    @IBOutlet private weak var lblMaker: UILabel!
    @IBOutlet private weak var lblCost: UILabel!
    @IBOutlet private weak var lblLength: UILabel!
    @IBOutlet private weak var lblSpeed: UILabel!
    @IBOutlet private weak var lblCrew: UILabel!
    @IBOutlet private weak var lblPassengers: UILabel!
    @IBOutlet private weak var btnNext: FadeEnableBtn!
    @IBOutlet private weak var btnPrevious: FadeEnableBtn!
    
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingSpinner: UIActivityIndicatorView!
    
    // MARK: Properties
    
    var person: Person?
    private let api = StarshipApi()
    private var starships = [String]()
    private var currentStarship = 0
    
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let person = person else { fatalError("\(#function): Person is nil") }
        guard person.starshipUrls.count > 0 else { fatalError("\(#function): Person doesn't have starships") }
        
        self.starships = person.starshipUrls
        setButtonState()
        downloadStarship(for: starships[currentStarship])
    }
    
    // MARK: IBActions
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        currentStarship += 1
        setButtonState()
        downloadStarship(for: starships[currentStarship])
    }
    
    @IBAction func btnPreviousTapped(_ sender: UIButton) {
        currentStarship -= 1
        setButtonState()
        downloadStarship(for: starships[currentStarship])
    }
    
    // MARK: Private
    
    private func updateUI(starship: Starship) {
        lblName.text = starship.name
        lblModel.text = starship.model
        lblMaker.text = starship.maker
        lblCost.text = starship.cost
        lblLength.text = starship.length
        lblSpeed.text = starship.speed
        lblCrew.text = starship.crew
        lblPassengers.text = starship.passengers
        loading(isEnabled: false)
    }
    
    private func downloadStarship(for starshipUrl: String) {
        loading(isEnabled: true)
        api.getStarship(from: starships[currentStarship]) { (starship) in
            if let starship = starship {
                DispatchQueue.main.async {
                    self.updateUI(starship: starship)
                }
            }
        }
    }
    
    private func setButtonState() {
        btnPrevious.isEnabled = currentStarship == 0 ? false : true
        btnNext.isEnabled = currentStarship == starships.count - 1 ? false : true
    }
    
    private func loading(isEnabled: Bool) {
        if isEnabled {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 1.0
                self.loadingView.isHidden = false
                self.loadingSpinner.startAnimating()
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 0.0
                self.loadingView.isHidden = true
                self.loadingSpinner.stopAnimating()
            }
        }
    }
}
