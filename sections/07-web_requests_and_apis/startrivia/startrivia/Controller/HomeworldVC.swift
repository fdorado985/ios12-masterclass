//
//  HomeworldVC.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class HomeworldVC: UIViewController, PersonProtocol {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var lblClimate: UILabel!
    @IBOutlet private weak var lblTerrain: UILabel!
    @IBOutlet private weak var lblPopulation: UILabel!
    
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingSpinner: UIActivityIndicatorView!
    
    // MARK: Properties
    
    var person: Person?
    private var api = HomeworldApi()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let person = person else { fatalError("\(#function): Person is nil") }
        loading(isEnabled: true)
        api.getHomeworld(url: person.homeworldUrl) { (homeworld) in
            if let homeworld = homeworld {
                DispatchQueue.main.async {
                    self.updateUI(homeworld: homeworld)
                }
            }
            
        }
    }
    
    // MARK: Private
    
    private func updateUI(homeworld: Homeworld) {
        lblName.text = homeworld.name
        lblClimate.text = homeworld.climate
        lblTerrain.text = homeworld.terrain
        lblPopulation.text = homeworld.population
        loading(isEnabled: false)
    }
    
    private func loading(isEnabled: Bool) {
        if isEnabled {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 1.0
                self.loadingView.isHidden = false
                self.loadingSpinner.startAnimating()
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 0.0
                self.loadingView.isHidden = true
                self.loadingSpinner.stopAnimating()
            }
        }
    }
}
