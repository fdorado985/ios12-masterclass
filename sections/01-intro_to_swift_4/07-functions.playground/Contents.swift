import UIKit

// MARK: Functions
// It is basically a set of instructions that accomplishes one specific task.

// Create a function
func declareName() {
    print("John")
}

// Call a function
declareName()

// Create a function (w/ parameters)
func declare(name: String) {
    print("Hello, \(name)")
}

// Call a function with parameters
declare(name: "John")

// Create a function with parameters and return type
func createFullName(firstName: String, lastName: String) -> String {
    return firstName + " " + lastName
}

let name = createFullName(firstName: "John", lastName: "Dorado")
print(name)
