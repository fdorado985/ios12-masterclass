//
//  HomeVC.swift
//  googeatins
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: Properties
    
    private let data = DataSet()
    private var categoryToPass: FoodCategory?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let recipesVC = segue.destination as? RecipesSelectionVC else { fatalError("RecipesSelectionVC was not found") }
        guard let categoryToPass = self.categoryToPass else { fatalError("categoryToPass is nil") }
        recipesVC.selectedCategory = categoryToPass
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as? CategoryCell else {
            fatalError("CategoryCell was not found")
        }
        
        let category = data.categories[indexPath.row]
        cell.configureCell(category: category)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        categoryToPass = data.categories[indexPath.row]
        performSegue(withIdentifier: "RecipesSelectionVC", sender: self)
    }
}

