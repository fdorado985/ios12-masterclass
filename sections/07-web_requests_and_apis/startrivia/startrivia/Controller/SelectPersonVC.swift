//
//  SelectPersonVC.swift
//  startrivia
//
//  Created by Juan Francisco Dorado Torres on 9/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

protocol PersonProtocol {
    var person: Person? { get set }
}

class SelectPersonVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var lblHeight: UILabel!
    @IBOutlet private weak var lblMass: UILabel!
    @IBOutlet private weak var lblHair: UILabel!
    @IBOutlet private weak var lblBirthYear: UILabel!
    @IBOutlet private weak var lblGender: UILabel!
    @IBOutlet private weak var btnHomeworld: UIButton!
    @IBOutlet private weak var btnVehicles: UIButton!
    @IBOutlet private weak var btnStarships: UIButton!
    @IBOutlet private weak var btnFilms: UIButton!
    
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingSpinner: UIActivityIndicatorView!
    
    // MARK: Properties
    
    private var person: Person?
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        btnHomeworld.isEnabled = false
        btnVehicles.isEnabled = false
        btnStarships.isEnabled = false
        btnFilms.isEnabled = false
        loading(isEnabled: false)
    }
    
    // MARK: IBActions
    
    @IBAction private func btnRandomTapped(_ sender: UIButton) {
        let randomId = Int.random(in: 1 ... 87)
        loading(isEnabled: true)
        PersonApi().getRandomPerson(id: randomId) { (person) in
            DispatchQueue.main.async {
                if let person = person {
                    self.updateUI(person: person)
                }
            }
        }
    }
    
    // MARK: Private
    
    private func updateUI(person: Person) {
        self.person = person
        lblName.text = person.name
        lblHeight.text = person.height
        lblMass.text = person.mass
        lblHair.text = person.hair
        lblBirthYear.text = person.birthYear
        lblGender.text = person.gender
        
        btnHomeworld.isEnabled = !person.homeworldUrl.isEmpty
        btnVehicles.isEnabled = !person.vehicleUrls.isEmpty
        btnStarships.isEnabled = !person.starshipUrls.isEmpty
        btnFilms.isEnabled = !person.filmUrls.isEmpty
        loading(isEnabled: false)
    }
    
    private func loading(isEnabled: Bool) {
        if isEnabled {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 1.0
                self.loadingView.isHidden = false
                self.loadingSpinner.startAnimating()
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 0.0
                self.loadingView.isHidden = true
                self.loadingSpinner.stopAnimating()
            }
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let person = self.person else { fatalError("\(#function): Person is nil") }
        guard var vc = segue.destination as? PersonProtocol else { fatalError("\(#function): PersonProtocol ViewController was not found") }
        vc.person = person
    }
}

