import UIKit

// MARK: - Inheritance

class Hero {
    
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}

class Warrior: Hero {
    
    var attackSkill = 50
    var defenseSkill = 15
    
    init(name: String, age: Int, attackSkill: Int, defenseSkill: Int) {
        super.init(name: name, age: age)
        self.attackSkill = attackSkill
        self.defenseSkill = defenseSkill
    }
    
    func attack() {
        attackSkill += 5
    }
}

class WarriorMonk: Warrior {
    
    var weaponsKnowledge = 200
    
    init(name: String, age: Int, attackSkill: Int, defenseSkill: Int, weaponsKnowledge: Int) {
        super.init(name: name, age: age, attackSkill: attackSkill, defenseSkill: defenseSkill)
        self.weaponsKnowledge = weaponsKnowledge
    }
    
    override func attack() {
        attackSkill += 10
    }
}

let warrior = Warrior(name: "Fenix", age: 25, attackSkill: 100, defenseSkill: 25)
warrior.attack()

let monk = WarriorMonk(name: "Monk", age: 20, attackSkill: 30, defenseSkill: 100, weaponsKnowledge: 250)
