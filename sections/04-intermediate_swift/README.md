# Intermediate Swift
On this list of playgrounds you will be able to see some of different new topics.

* Optionals
* Classes
* Class Initializers
* Structs
* Differences between structs and classes
* Inheritance
* Enumerations
