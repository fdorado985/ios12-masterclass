//
//  FoodCategory.swift
//  googeatins
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct FoodCategory {
    
    // MARK: Properties
    
    let title: String
    let imageName: String
}
