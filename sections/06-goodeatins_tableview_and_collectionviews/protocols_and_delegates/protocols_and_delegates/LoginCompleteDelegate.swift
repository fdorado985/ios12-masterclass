//
//  LoginCompleteDelegate.swift
//  protocols_and_delegates
//
//  Created by Juan Francisco Dorado Torres on 9/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

protocol LoginCompleteDelegate {
    
    // MARK: Required Functions
    
    func userCreate(name: String)
}
