import UIKit

// MARK: Control Flow & Loops

// For Loop

// In Array
let names = ["Peter", "Paul", "Mary", "Frank"]
for name in names {
    print("Hello, \(name)")
}

// In Dictionaries
let numberOfLegs = ["Spider" : 8, "Dog" : 4, "Human" : 2]
for (animalType, legCount) in numberOfLegs {
    print("\(animalType)s have \(legCount) legs.")
}

// In Numbers
for index in 1 ... 5 {
    print("\(index) times 5 is equal to \(index * 5)")
}

for index in 0 ..< 4 {
    print(names[index])
}

// While Loops

var score = 0
var diceRoll = 0

while score < 20 {
    diceRoll = Int(arc4random_uniform(6))
    print("DICE ROLL: ", diceRoll, "\n")
    
    if diceRoll <= 4 {
        score = score + 1
        print("SCORE:", score)
    }
}
